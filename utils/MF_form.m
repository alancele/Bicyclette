% tracking characteristic of the standard Magic Formula
function [Fx,Fy,Mz]=MF_form(Fz,sigma,alpha,Gamma)
%#codegen
 % Fz [kN] load on a wheel in nominal cond. 
 % slip: slip ratio
 % alpha: slip angle
 % Gamma: camber angle
% D=100; C=1; B=1; C=1.5; E=0.2;
a=[1.6929, -55.2084, 1271.28, 1601.8, 6.4946, 4.7966*10^-3, -0.3875, 1, 0, 0, 0, 0, 0, 0, 0];
b=[1.65, -7.6118, 1122.6, -7.36*10^-3, 144.82, -7.6614*10^-2, -3.86*10^-3, 8.5055*10^-2, 7.5719*10^-2, 2.3655*10^-2, 2.3655*10^-2,0,0,0];
c=[2.2264, -3.0428, -9.2284, 0.5, -5.56696, -0.25964, -1.29724^-3, -.358348, 3.74476, -15.15166, 2.1156*10^-3, 3.46*10^-4,   9.13952*10^-3, -0.244556, 0.100695, -1.3980, 0.44441, -0.998344];

 % Combined coefficients RBx1,RBx2,RCx1,RBy1,RBy2,RCy1
 d =[10.0,4.0,0.4,6.0,2.0,0.4];
%   d =[10.0,4.0,0.2,6.0,2.0,0.4]; work well for bicycle case but MapleSim
%   d =[10.0,6.0,1.0,16.0,0.0,1.0]; 
% Fx
      C=b(1);
      upx=b(2)*Fz+b(3);
      D=upx*Fz;
      B=(b(4)*Fz^2+b(5)*Fz)*exp(-b(6)*Fz)/(C*D);
      E=(b(7)*Fz^2+b(8)*Fz+b(9));
      Fx0=D*sin(C*atan(B*(1-E)*sigma+E*atan(B*sigma)));
% Fy
      C=a(1);
      upy=a(2)*Fz+a(3);
      D=upy*Fz;
      B=a(4)*sin(2*atan(Fz/a(5))*(1-a(6)*Gamma))/(D*C);
      E=a(7)*Fz+a(8);
      Fy0=D*sin(C*atan(B*(1-E)*alpha+E*atan(B*alpha)));
      
%       Fx=Fx0;
%       Fy=Fy0;
      % wrong
%       Fx=Fx0.*sqrt(1-(Fy0./(upy*Fz)).^2);
%       Fy=Fy0.*sqrt(1-(Fx0./(upx*Fz)).^2);
% combined
      B  = d(1) * cos(atan(d(2) * sigma/100));
      C  = d(3); 
      S  = alpha*pi/180; 
      Gx  = cos(C.* atan(B.* S));

      B  = d(4) * cos(atan(d(5) * (alpha*pi/180)));
      C  = d(6); 
      S  = sigma/100;
      Gy = cos(C .* atan(B .* S));  

      Fx = Gx .* Fx0;
      Fy = Gy .* Fy0;
% Mz 
%       Shz = c(12)*Fz+c(13)+c(14)*Gamma;
%       Svz = c(15)*Fz+c(16)+(c(17)*Fz*Fz+c(18)*Fz)*Gamma;
      C=c(1);
      D=c(2)*Fz^2+c(3)*Fz;
      E=(c(8)*Fz^2+c(9)*Fz+c(10))/(1-c(11)*Gamma);
      B=(c(4)*Fz^2+c(5)*Fz)*(1-c(7)*Gamma)*(exp(-c(6)*Fz))/(C*D);
      
      Mz=D.*sin(C.*atan(B.*(1-E).*(alpha)+E.*atan(B.*(alpha))));
end












