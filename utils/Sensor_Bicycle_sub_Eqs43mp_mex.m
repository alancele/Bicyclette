function WheelFext=Sensor_Bicycle_sub_Eqs43mp_mex(x,u,bicycle_params)
%#codegen
coder.extrinsic('disp');
    %calculate the forces pnematics: Fxf,Fyf,Mzf,Fxr,Fyr,Myr
    %The vertical forces
    Fzr=0.5*bicycle_params.m*bicycle_params.g*bicycle_params.Lf/(bicycle_params.Lr+bicycle_params.Lf);
    Fzf=0.5*bicycle_params.m*bicycle_params.g*bicycle_params.Lr/(bicycle_params.Lr+bicycle_params.Lf);
   
    delta =u(1);
%The lateral angles
    dtheta=x(3);
    Vx=x(1);
    Vy=x(2);
     
    vth=0.01;
    dphi1=x(4);
    dphi2=x(5);
    
    denom = max(abs(Vx),vth);
    alpha_r= -atan((Vy-bicycle_params.Lr*dtheta)/denom);
    
    alpha_f= delta-atan((Vy+bicycle_params.Lf*dtheta)/denom);
    if Vx^2<=vth^2
        alpha_r=0;
    end
    if Vx^2<=vth^2
        alpha_f=0;
    end
     
    %The slip-ratio
    Vxr=Vx;
    denom = max([abs(Vxr),abs(bicycle_params.R*dphi2),vth]);
    sigma_r=(bicycle_params.R*dphi2-Vxr)/denom;
    
    if(abs(sigma_r)>1)
        sigma_r=sign(sigma_r);
    end
    %The slip-ratio 
    Vxb=Vx;
    Vyb=Vy+bicycle_params.Lf*dtheta;
    Vxf=Vxb*cos(delta)+Vyb*sin(delta);%
    denom = max([abs(Vxf),abs(bicycle_params.R*dphi1),vth]);
    sigma_f=(bicycle_params.R*dphi1-Vxf)/denom;
   
    if(abs(sigma_f)>1)
        sigma_f=sign(sigma_f);
    end
%      disp(sigma_r);
    [Fx,Fy,Mz]=MF_form(Fzf/1000,sigma_f*100,alpha_f*180/pi,0);
    Fxf=Fx;
    Fyf= Fy;
    Mzf= Mz;
    [Fx,Fy,Mz]=MF_form(Fzr/1000,sigma_r*100,alpha_r*180/pi,0);
    Fxr= Fx;
    Fyr= Fy;
    Mzr= Mz;
%     disp(Mzr+Mzf);
    WheelFext=[Fxf, Fyf, Mzf, Fxr, Fyr, Mzr,sigma_f*100,sigma_r*100,alpha_f*180/pi,alpha_r*180/pi];
end