% MAKECOLVECTOR - Makes sure a vector is a column vector
%
% function x = makecolvector(x)
%
% Checks if x is a column vector. If it is not, x is transposed.

function vx = makecolvector(x)
%#codegen

[n, m] = size(x);
vx=x;
if (n < m)
    vx = x';
end
end