function [idx, w] = ndprelookup(NDDataDef, Z) %#codegen
% [idx, w] = ndprelookup(NDData, X) - Builds interpolator
% 
% Build piecewise interpolator for point X. Returns the linear indices 
% of the hypercube vertices and their corresponding weights w.
% New: determine the length of X

coder.extrinsic('warning')

assert(NDDataDef.dim <= 9);


Zdim  = NDDataDef.dim; 
Zsize = NDDataDef.size; 
t     = zeros(Zdim, 1);
coord = zeros(Zdim, 1);
idx   = zeros(Zdim + 1, 1);
w     = zeros(Zdim + 1, 1);


% calcul de la distance entre les deux premiers points ; 
% pour une grille régulière sinon adapter le calcul en chaque point
% a ete deplace pour calcul en vecteur, bizarrement corrige un bug dans le
% calcul des index ; potentiellement un problème de précision
gs = abs(NDDataDef.Z(:,1) - NDDataDef.Z(:,2));

% disp(Xdim);
for i = 1:Zdim
    
    if Z(i) < NDDataDef.Z(i,1) || Z(i) > NDDataDef.Z(i,Zsize(i))
        warning('ndprelookup:outofrange', 'extrapolation needed');  
        % on borne sur la grille
        Z(i) = min( max( Z(i), NDDataDef.Z(i,1) ), NDDataDef.Z(i,Zsize(i)) );
    end
    % calcul de l'index de la coordonnée isur grille régulière
    ratio = (Z(i) - NDDataDef.Z(i,1)) / gs(i);
    coord(i) = floor(ratio)+1; % ceil (round plus infinity) c'est pour les index Matlab qui commence à 1 et non à 0
    if (coord(i) >= Zsize(i))
        coord(i) = Zsize(i)- 1;
    end
   t(i) = ratio - (coord(i) - 1);
end
 
% le tri permet la mise en place de l'algorithme de calcul des poids pour
% une approximation barycentrique
[dummy, p] = sort(t, 1, 'descend');


k = sub2ind2(Zsize', coord);

idx(1) = k;
w(1) = 1;

for i = 1:Zdim
    j = p(i);
    w(i) = w(i) - t(j);
    w(i + 1) = t(j);
    coord(j) = coord(j) + 1;
    idx(i + 1) = sub2ind2(Zsize', coord);
end

end
%endfunction

