f   = 0.1; 
x0 = pi/2;
dx0  = 0;

T0 = 0;

yref = [10 5 10];

Te = 0.05

tau = 0.3
w0 = 1/tau
z = 1.0
surface = tf([w0^2],[1 2*z*w0 w0^2])
% surface = tf([1],[tau 1])

Hd = c2d(surface,Te)
Num = Hd.Numerator{1};
Den = Hd.Denominator{1};

% on a S = Den
S = Den'