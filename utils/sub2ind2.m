function ndx = sub2ind2(siz,v)
%  #codegen
%  sub2ind2 Linear index from multiple subscripts.
%   sub2ind2 is used to determine the equivalent single index
%   corresponding to a given set of subscript values.
%
%   IND = SUB2IND(SIZ,[I1,I2,...,IN]) returns the linear index
%   equivalent to the N subscripts in the arrays I1,I2,...,IN for an
%   array of size SIZ. 
%
%   See also IND2SUB2.

%   Copyright 2014 huu-phuc.nguyen@hds.utc.fr, Lab Heudiasyc.
%   $Revision: 1.14.4.10 $  $Date: 2014/18/06 18:27:12 $

siz = double(siz);
lensiz = length(siz);
numOfIndInput = length(v);
if lensiz ~= numOfIndInput
    ndx=0; 
else
    %Compute linear indices
    fact  = 1;
    ndx = 1;
    for i = 1:numOfIndInput
        vi    = v(i);
        ndx = ndx + (vi-1)*fact;
        fact = fact*siz(i);
    end
end
end
