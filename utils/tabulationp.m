% TABULATION - Calcul de l'evolution du systeme et construction des tables.
%
% function tabulation()
% function tabulation('Nom de fichier')
%
% Calcule l'evolution du systeme defini par X' = f(X, U), avec X E Rn et
% U E Rm pour toutes les combinaisons de conditions initiales X0 et
% d'entrees U. Le systeme est defini dans le fichier SIMULINK calcul.mdl
% par defaut, ou par le fichier dont le nom est passe en parametre.
% nouvelle version par NGUYEN Huu Phuc 12/2013
function tbl = tabulationp()

global UIntervalle;
global XIntervalle;
global UQuant;
global XQuant;
% global Te;
global YToRef;
global YToX;


disp('Allocation memoire...');
% Les entrees et variables d'etat peuvent avoir une quantification differente.
% Par consequent, chaque ensemble de valeurs possibles est stocke dans une cellule 
% differente. ie, Tables.X{1} = x, Tables.X{2} = dx/dt, Tables.X{3} = d2x/dt2 ...

tbl                    = struct('param', [], 'dataout', []);
tbl.param              = struct('Zdef', [],'Xdef', [], 'Ydef', [], 'Udef', [], ...
                                'YToRef', [], 'YToX', [],'dataoutdim', []);
tbl.param.Xdef         = struct('dim', 0, 'size', 0, 'Idx', []);
tbl.param.Udef         = struct('dim', 0, 'size', 0, 'Idx', []);
tbl.param.Ydef         = struct('dim', 0, 'size', 0, 'Idx', 0);
tbl.param.Zdef         = struct('Z', [], 'size', 0, 'dim', 0);

tbl.param.Udef.dim    = size(UIntervalle, 1);

tbl.param.Xdef.dim    = size(XIntervalle, 1);
tbl.param.Ydef.dim    = size(YToRef, 1);         %1 or 2

tbl.param.YToRef       = YToRef;
tbl.param.YToX         = YToX;
tbl.param.dataoutdim = ones(tbl.param.Ydef.dim,1);

n = tbl.param.Udef.dim + tbl.param.Xdef.dim;
disp(n)
tbl.param.Zdef.dim = n;
tbl.param.Zdef.size = zeros(n, 1);

%tbl.param.Te = Te;
intervalles = [UIntervalle; XIntervalle];
quant = [UQuant ; XQuant];

values = cell(n, 1);

length_temp = max(tbl.param.Zdef.size);
tbl.param.Zdef.Z = zeros(n,length_temp);
for i = 1:n
    values{i} = intervalles(i, 1):quant(i):intervalles(i, 2);
    temp = transpose(makecolvector(values{i}));

    if i<= tbl.param.Udef.dim
        tbl.param.Udef.size(i) = length(temp);
        tbl.param.Udef.Idx(i,(1:tbl.param.Udef.size(i)))   = (1:tbl.param.Udef.size(i));
    else
        tbl.param.Xdef.size(i-tbl.param.Udef.dim) = length(temp);
        tbl.param.Xdef.Idx(i-tbl.param.Udef.dim,(1:tbl.param.Xdef.size(i-tbl.param.Udef.dim)))   = (1:tbl.param.Xdef.size(i-tbl.param.Udef.dim));
    end
    tbl.param.Zdef.size(i) = length(temp);
    tbl.param.Zdef.Z(i,1:tbl.param.Zdef.size(i)) = temp;
    
end

% Allocate memory for data arrays (speeds up further accesses)
tbl.param.Zdef.size'
length(tbl.param.dataoutdim)
tbl.dataout = zeros([tbl.param.Zdef.size',length(tbl.param.dataoutdim)])

tbl.param.datanum = numel(tbl.dataout)/length(tbl.param.dataoutdim);

disp('Terminate.');



    


