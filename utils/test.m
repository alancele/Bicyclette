
Te = 0.05
% set_param('sim_Pend_IO_V0','SignalLogging','on')
% ph = get_param('sim_Pend_IO_V0/DATAS/Integrator1','PortHandles')
% set_param(ph.Outport(1),'DataLogging','on')

tau = 0.3
w0 = 1/tau;
z = 1.0;
surface = tf([w0^2],[1 2*z*w0 w0^2]);
% surface = tf([1],[tau 1])

Hd = c2d(surface,Te);
Num = Hd.Numerator{1};
Den = Hd.Denominator{1};

% on a S = Den
S = Den';


for i=TablesOneL.param.Xdef.Idx(1,(1:TablesOneL.param.Xdef.size(1)))
    Vx0 = TablesOneL.param.Zdef.Z(3,i);
    for j=TablesOneL.param.Xdef.Idx(2,(1:TablesOneL.param.Xdef.size(2)))
        Vy0 = TablesOneL.param.Zdef.Z(4,j);
        for k = TablesOneL.param.Xdef.Idx(3,(1:TablesOneL.param.Xdef.size(3)))
            dtheta0 = TablesOneL.param.Zdef.Z(5,k);
            for l = TablesOneL.param.Xdef.Idx(4,(1:TablesOneL.param.Xdef.size(4)))
                dVx0 = TablesOneL.param.Zdef.Z(6,l);
                for m = TablesOneL.param.Xdef.Idx(5,(1:TablesOneL.param.Xdef.size(5)))
                    dVy0 = TablesOneL.param.Zdef.Z(7,m);
                    for n = TablesOneL.param.Xdef.Idx(6,(1:TablesOneL.param.Xdef.size(6)))
                        d2theta0 = TablesOneL.param.Zdef.Z(8,n);
                        for p = TablesOneL.param.Udef.Idx(1,(1:TablesOneL.param.Udef.size(1)))
                            delta0 = TablesOneL.param.Zdef.Z(1,p);
                            for q = TablesOneL.param.Udef.Idx(2,(1:TablesOneL.param.Udef.size(2)))
                                Mr0 = TablesOneL.param.Zdef.Z(2,q);
                                dx0_new=zeros(3,1);
                                x0 = [Vx0, Vy0, dtheta0, dVx0, dVy0, d2theta0];
                                u0 =[delta0; Mr0];
                                y0 = Sensor_Bicycle_sub_Eqs43mp_mex([Vx0, Vy0, dtheta0, ....
                                    dVx0, dVy0 d2theta0]', [delta0, Mr0]' ,bicycle_params);
                                Vx=Vx0;
                                Vy=Vy0;
                                dtheta=dtheta0;
                                Fxf=y0(1);
                                Fyf=y0(2);
                                Mzf=y0(3); 
                                Fxr=y0(4); 
                                Fyr=y0(5);
                                Mzr=y0(6);
                         
                                dx0_new(1)=Vy0*dtheta0+2/bicycle_params.m*(Fxr+Fxf*cos(delta0)-Fyf*sin(delta0))-0.1*Vx0^2;
                                dx0_new(2)=-Vx0*dtheta0+2/bicycle_params.m*(Fyr+Fxf*sin(delta0)+Fyf*cos(delta0));
                                dx0_new(3)=2/bicycle_params.Iz*(-Fyr*bicycle_params.Lr+(Fxf*sin(delta0)+Fyf*cos(delta0))*bicycle_params.Lf+Mzr+Mzf);     
                                TablesOneL.dataout(p, q, i, j, k, l, m, n, :) = dx0_new; % remplacer dx0_new par x0_new-x0

                            end
                        end
                    end
                end
            end
        end
    end
    disp(i)
end
    
% x0=pi/2
% dx0 = 0