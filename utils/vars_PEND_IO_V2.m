% modified 11/03/2016
% simple altitude function y' = -tanh(y+u^3);
% using input-output model
% define deltaYk = y(k)-y(k-1)

%clc;
%clear;
global XIntervalle; % Intervalles de l'etat
global XQuant;      % Quantification de l'etat
global UIntervalle; % Intervalles des entrees
global UQuant;      % Quantification des entrees
% global Te;          % Periode d'echantillonnage    
% 
% global A;
global YToX;
global YToRef;
global XToY;

                    
% Variables
Te = 0.01; 

% Intervalles d'etat consideres 
% pour dx,dy,dtheta,dphi1,dphi2
%I_a_k I_b_k I_a_k-1 I_b_k-1 V_a_k-1 V_b_k-1 
XIntervalle = [-130 130 ; -130 130; -2*pi 2*pi; -130 130 ; -130 130; -2*pi 2*pi];  
XQuant = (XIntervalle(:,2)-XIntervalle(:,1))./[ 7; 7; 7; 7; 7; 7]; 
% pour u1 dans une intervalle
%V_a_k V_b_k w
UIntervalle = [-2*pi 2*pi ; 0 200];
UQuant  = (UIntervalle(:,2)-UIntervalle(:,1))./[ 7; 7];

YToX    = eye(3);
                  
YToRef  = eye(3);

XToY    = eye(3); 
% Placer ci dessous les variables propres au systeme.
% ---------------------------------------------------
 
% Systeme lineaire a suivre localement

%solve state function
% 

%TablesOne = zeros([(XIntervalle(:,2)-XIntervalle(:,1))./XQuant;(UIntervalle(:,2)-UIntervalle(:,1))./UQuant]');
if exist("TablesOneL", 'var') 
    clear("TablesOneL")
end
%TablesOne = zeros([(XIntervalle(:,2)-XIntervalle(:,1))./XQuant;(UIntervalle(:,2)-UIntervalle(:,1))./UQuant]');
TablesOneL = tabulationp();
dataout = TablesOneL.dataout;




